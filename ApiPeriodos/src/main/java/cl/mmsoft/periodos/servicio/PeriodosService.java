package cl.mmsoft.periodos.servicio;

import cl.mmsoft.periodos.swagger.codegen.model.Periodo;
import cl.mmsoft.periodos.tools.RandomDate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author mig_s
 */
@Service
public class PeriodosService {
    private final static int MIN = 25;
    private final static int MAX = 90;
    
    public List<Periodo> getPeriodos(){
        RandomDate fechaInicial = new RandomDate(LocalDate.of(1960, 1, 1), LocalDate.of(2013, 1, 1));
        RandomDate fechaFin = new RandomDate(LocalDate.of(2013, 2, 1), LocalDate.of(2019, 1, 1));
        
        List<Periodo> periodos = new ArrayList();
        for (long i = 1l; i <= 100l; i++) {
            Periodo periodo = new Periodo();
            periodo.setId(i);
            periodo.setFechaCreacion(fechaInicial.nextDate());
            periodo.setFechaFin(fechaFin.nextDate());
            RandomDate fechaPeriodos = new RandomDate(periodo.getFechaCreacion(), periodo.getFechaFin());
            Random r = new Random();
            int cantidadPeriodos = r.nextInt((MAX - MIN) + 1) + MIN;
            List<LocalDate> fechas = new ArrayList();
            for (int e = 1; e < cantidadPeriodos; e++) {
                fechas.add(fechaPeriodos.nextDate());
            }
            periodo.setFechas(fechas.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList()));
            periodos.add(periodo);
        }
        
        return periodos;
    }
}

